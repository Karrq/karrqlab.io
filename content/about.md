+++
title="About"
date=2019-07-18T12:00:00Z

[extra]
hidden=true
+++

Welcome to my corner of the web. You will find (arguably) useful material, mostly related to programming.

This website is made with [Zola](https://www.getzola.org/), hosted with [GitLab Pages](https://gitlab.com/Karrq/karrq.gitlab.io).

### Get in touch
* [GitLab](https://gitlab.com/Karrq/)